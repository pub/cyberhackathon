#ifndef N
#error This program requires 'N' to be defined.
#endif

int main() {
  int random;
  if (random < N) {
    return 1;
  } else {
    return 0;
  }
}
